#!/usr/bin/env python3

"""
TensorFlow GAN
==============

```
k  = filter width
m  = filter count
n  = input dimension
u  = noise dimension
r  = stride/dilation
n' = n / r
```

discriminator
-------------

```
input
 [n]
  | tf.nn.conv1d + relu
  | k x 1 x m filters
  v
 [n x m]
  | tf.nn.conv1d + relu
  | k x m x m filters with stride r
  v
 [n' x m]
  | tf.matmul + sigmoid
  | (m*n') x 1 fully connected layer
  v
 [1]
```

generator
---------

```
noise
 [u]
  | tf.matmul + reshape
  | u x u fully connected layer
  v
 [n' x m]
  | tf.nn.conv1d_transpose + relu
  | k x m x m filters with dilation r
  v
 [n x m]
  | tf.nn.conv1d_transpose + relu
  | k x 1 x m filters
  v
 [n]
```
"""

import os
import time

import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf

print ("gan main...")

#tf.compat.v1.enable_eager_execution()
tf.compat.v1.disable_eager_execution()

print ("tensorflow version: ", tf.version.VERSION)
print ("eager execution:    ", tf.executing_eagerly())
np.set_printoptions (linewidth=200)

#
# dimensions and constants
#

nepochs           = 1001
nsamples          = 20
batch_size        = 20
input_dimension   = 20
filter_dimension  = 5
filter_count      = 8
stride            = 2
dilation          = 2
weights_dimension = int ((filter_count * input_dimension) / stride)
noise_dimension   = 80
rate              = 0.01

print ("nsamples:           ", nsamples)
print ("input_dimension:    ", input_dimension)
print ("filter_dimension:   ", filter_dimension)
print ("filter_count:       ", filter_count)
print ("stride:             ", stride)
print ("weights_dimension:  ", weights_dimension)

#
# load dataset and initial parameters
#

print ("loading random input.bin...")
input = np.fromfile ("input.bin", dtype='float32').reshape (
  (1, input_dimension, 1))
print ("input: ", input.shape, '\n', input)
label = np.array ([[[0.0]]], dtype='float32')
print ("label: ", label.shape, '\n', label)
print ("loading random conv1d.bin...")
conv1d = np.fromfile ("conv1d.bin", dtype='float32').reshape (
  (filter_count, 1, filter_dimension)).transpose()
print ("conv1d: ", conv1d.shape, '\n', conv1d)
print ("loading random downconv1d.bin...")
downconv1d = np.transpose (
  np.fromfile ("downconv1d.bin", dtype='float32').reshape (
    (filter_count, filter_dimension, filter_count)),
  [1, 2, 0])
print ("downconv1d: ", downconv1d.shape, '\n', downconv1d)
print ("loading random linear.bin...")
linear = np.fromfile ("linear.bin", dtype='float32').reshape (
  (weights_dimension, 1))
print ("linear: ", linear.shape, '\n', linear)

"""
print ("bias vectors...")
#bvec1 = np.random.default_rng().random (filter_count, dtype='float32')
bvec1 = np.zeros (filter_count, dtype='float32')
print ("bvec1: ", bvec1)
#bvec2 = np.random.default_rng().random (filter_count, dtype='float32')
bvec2 = np.zeros (filter_count, dtype='float32')
print ("bvec2: ", bvec2)
#bvec3 = np.random.default_rng().random (1, dtype='float32')
bvec3 = np.zeros (1, dtype='float32')
print ("bvec3: ", bvec3)
"""

print ("loading inputs.bin...")
inputs = np.fromfile ("inputs.bin", dtype='float32').reshape (
  (nsamples, input_dimension, 1))
#print ("inputs: ", inputs.shape, '\n', inputs)
print ("loading labels.bin...")
labels = np.fromfile ("labels.bin", dtype='float32').reshape (
  (nsamples, 1, 1))
#print ("labels: ", labels.shape, '\n', labels)

#
# computation graph
#
input1    = tf.compat.v1.placeholder ('float32',
  shape=(batch_size, input_dimension, 1))
filters1  = tf.Variable (tf.constant (conv1d))
out1      = tf.nn.relu (
  tf.nn.conv1d (input1, filters1, stride=None, padding='SAME'))
"""
bias1     = tf.Variable (tf.constant (bvec1))
out1      = tf.nn.relu (tf.nn.bias_add (
  tf.nn.conv1d (input1, filters1, padding='SAME'), bias1))
"""
filters2  = tf.Variable (tf.constant (downconv1d))
# NOTE: using a relu activation on this layer seemed to slow convergence in the
# test case
out2      = tf.nn.conv1d (out1, filters2, stride=2, padding='SAME')
"""
bias2     = tf.Variable (tf.constant (bvec2))
out2      = tf.nn.relu (tf.nn.bias_add (
  tf.nn.conv1d (out1, filters2, stride=2, padding='SAME'), bias2))
"""
weights   = tf.Variable (tf.constant (linear))
dense     = tf.matmul (tf.reshape (out2, [batch_size, 1, -1]), weights)
out3      = tf.nn.sigmoid (
  tf.matmul (tf.reshape (out2, [batch_size, 1, -1]), weights))
"""
bias3     = tf.Variable (tf.constant (bvec3))
out3      = tf.nn.bias_add (
  tf.matmul (tf.reshape (out2, [20, 1, -1]), weights), bias3)
"""
# the output of the discriminator
sigmoid   = tf.nn.sigmoid (dense)
test      = tf.math.reduce_sum (
  tf.nn.sigmoid_cross_entropy_with_logits (logits=dense, labels=labels))
# discriminator training
optimizer = tf.compat.v1.train.GradientDescentOptimizer (rate)
train     = optimizer.minimize (test)

#
# generator
#
random_g   = np.array (list (map (lambda x: 2.0 * x - 1.0,
  np.random.default_rng().random (
    noise_dimension, dtype='float32'
  )))).reshape ((1, noise_dimension))
input1_g   = tf.compat.v1.placeholder ('float32', shape=(1, noise_dimension))
linear_g   = np.random.default_rng().random (
  noise_dimension * noise_dimension, dtype='float32'
).reshape ((noise_dimension, noise_dimension))
weights_g  = tf.Variable (tf.constant (linear_g))
out1_g     = tf.reshape (tf.nn.relu (tf.matmul (input1_g, weights_g)), [
  1, int(input_dimension / dilation), filter_count
])
upconv1d   = np.random.default_rng().random (
  filter_dimension * filter_count * filter_count, dtype='float32'
).reshape ((filter_dimension, filter_count, filter_count))
filters1_g = tf.Variable (tf.constant (upconv1d))
out2_g     = tf.nn.relu (tf.nn.conv1d_transpose (
  out1_g, filters1_g, [1, input_dimension, filter_count], stride))

conv1d_transpose = np.random.default_rng().random (
  filter_dimension * filter_count, dtype='float32'
).reshape ((filter_dimension, 1, filter_count))
filters2_g = tf.Variable (tf.constant (conv1d_transpose))
out3_g = tf.nn.conv1d_transpose (
  out2_g, filters2_g, output_shape=[1, input_dimension, 1], strides=[1])

#
# run session
#

with tf.compat.v1.Session() as sess:
  init = tf.compat.v1.global_variables_initializer()
  feed_dict = { input1: inputs, input1_g: random_g }
  print ("init: ", sess.run (init))

  """
  # generator
  print ("generator...")
  print ("running fully connected layer...")
  result1_g = sess.run (out1_g, feed_dict)
  print ("result1_g: ", result1_g.shape, '\n', result1_g)
  print ("running upconv1d layer...")
  result2_g = sess.run (out2_g, feed_dict)
  print ("result2_g: ", result2_g.shape, '\n', result2_g)
  print ("running conv1d transpose layer...")
  result3_g = sess.run (out3_g, feed_dict)
  print ("result3_g: ", result3_g.shape, '\n', result3_g)
  """

  # discriminator
  print ("discriminator...")
  print ("running conv1d layer...")
  result1 = sess.run (out1, feed_dict)
  print ("result1: ", result1.shape, '\n', result1)
  gradients1 = sess.run (tf.gradients (out1, [input1, filters1]), feed_dict)
  print ("gradients1.input: ", gradients1[0].shape, '\n', gradients1[0])
  print ("gradients1.filters: ", gradients1[1].shape, '\n', gradients1[1])
  print ("running downconv1d layer...")
  result2 = sess.run (out2, feed_dict)
  print ("result2: ", result2.shape, '\n', result2)
  gradients2 = sess.run (tf.gradients (out2, [out1, filters2]), feed_dict)
  print ("gradients2.input: ", gradients2[0].shape, '\n', gradients2[0])
  print ("gradients2.filters: ", gradients2[1].shape, '\n', gradients2[1])
  #flattened = sess.run (flattened, feed_dict)
  #print ("flattened: ", flattened.shape, '\n', flattened)
  #dense = sess.run (dense, feed_dict)
  #print ("dense: ", dense.shape, '\n', dense)
  print ("running fully connected layer...")
  result3 = sess.run (out3, feed_dict)
  print ("result3: ", result3.shape, '\n', result3)
  gradients3 = sess.run (tf.gradients (out3, [out2, weights]), feed_dict)
  print ("gradients3.input: ", gradients3[0].shape, '\n', gradients3[0])
  print ("gradients3.filters: ", gradients3[1].shape, '\n', gradients3[1])
  print ("running discriminator...")
  discriminator = sess.run (sigmoid, feed_dict)
  print ("result: ", discriminator.shape, '\n', discriminator)
  gradients_final = sess.run (
    tf.gradients (sigmoid, [filters1, filters2, weights]), feed_dict)
  print ("gradients_final.filters1: ",
    gradients_final[0].shape, '\n', gradients_final[0])
  print ("gradients_final.filters2: ",
    gradients_final[1].shape, '\n', gradients_final[1])
  print ("gradients_final.weights: ",
    gradients_final[2].shape, '\n', gradients_final[2])
  print ("testing discriminator...")
  loss = sess.run (test, feed_dict)
  print ("loss: ", loss)
  gradient = sess.run (tf.gradients (test, [filters1, filters2, weights]),
    feed_dict)
  print ("gradient.filters1: ", gradient[0].shape, '\n', gradient[0])
  print ("gradient.filters2: ", gradient[1].shape, '\n', gradient[1])
  print ("gradient.weights: ", gradient[2].shape, '\n', gradient[2])

  print ("training for", nepochs, "epochs")
  t_start = time.perf_counter()
  losses = []
  for step in range (nepochs):
    sess.run (train, feed_dict)
    loss = sess.run (test, feed_dict)
    losses.append (loss)
    if step % 100 == 0:
      print ("step[", step, "]: ", loss)
  t_end = time.perf_counter()
  t_elapsed = t_end - t_start
  #print ("losses:", losses)
  print ("elapsed:", str (t_elapsed) + "s")

  xs = range (0, len (losses))
  fig, ax = plt.subplots()
  ax.plot (xs, losses)
  fig.savefig ("plot.png")
  os.system ("feh plot.png")

print ("...gan main")
